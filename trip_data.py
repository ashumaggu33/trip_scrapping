#!/usr/bin/env python
# coding: utf-8

import requests
import pandas as pd
import re
from datetime import datetime
from multiprocessing import Pool, Manager

headers = {'Postman-Token': '8c4a647f-8e16-00e1-cc34-a5a995342199'}
# Trip Website
base_url = 'https://www.trip.com'

# Categories
hotel_list_item = 'https://www.trip.com/restapi/soa2/16709/json/HotelSearch?testab=30eb30997603372beebf9ea2780e0c68dfe8b076fbfb1a82d09e8fa6a51cbaad'
hotel_detail = 'https://www.trip.com/restapi/soa2/16709/json/rateplan?testab=b9dd7b63e5d06571854028fb427b73b0011f35b614cccfcda256518507e1f581'
payload_request = {
  "seqid": "",
  "deduplication": [],
  "filterCondition": {
    "star": [],
    "rate": "",
    "priceRange": {
      "lowPrice": 0,
      "highPrice": -1
    },
    "priceType": 0,
    "breakfast": [],
    "payType": [],
    "bedType": [],
    "bookPolicy": [],
    "bookable": [],
    "discount": [],
    "zone": [],
    "landmark": [],
    "metro": [],
    "airportTrainstation": [],
    "location": [],
    "cityId": [],
    "amenty": [],
    "category": [],
    "feature": [],
    "brand": [],
    "popularFilters": [],
    "hotArea": []
  },
  "searchCondition": {
    "sortType": "AppointRank",
    "adult": 2,
    "child": 0,
    "age": "",
    "pageNo": 1,
    "optionType": "City",
    "optionId": "1",
    "lat": 0,
    "destination": "",
    "keyword": "",
    "cityName": "Beijing",
    "lng": 0,
    "cityId": 1,
    "checkIn": "2020-11-18",
    "checkOut": "2020-11-25",
    "roomNum": 1,
    "mapType": "gg",
    "travelPurpose": 0,
    "countryId": 1,
    "url": "https://www.trip.com/hotels/list?city=1&countryId=1&checkin=2020/11/18&checkout=2020/11/25&optionId=1&optionType=City&directSearch=0&display=Beijing&crn=1&adult=2&children=0&searchBoxArg=t&travelPurpose=0&ctm_ref=ix_sb_dl&domestic=1",
    "pageSize": 1000,
    "timeOffset": 28800,
    "radius": 0,
    "directSearch": 0
  },
  "meta": {
    "fgt": "",
    "hotelId": "",
    "priceToleranceData": "",
    "priceToleranceDataValidationCode": "",
    "mpRoom": [],
    "hotelUniqueKey": "",
    "shoppingid": "",
    "minPrice": "",
    "minCurr": ""
  },
  "queryTag": "NORMAL",
  "genk": "true",
  "genKeyParam": "a=0,b=2020-11-18,c=2020-11-25,d=en-xx,e=2",
  "webpSupport": "true",
  "platform": "online",
  "pageID": "10320668148",
  "head": {
    "Version": "",
    "userRegion": "XX",
    "Locale": "en-XX",
    "LocaleController": "en-XX",
    "TimeZone": "5.5",
    "Currency": "CNY",
    "PageId": "10320668148",
    "webpSupport": "true",
    "userIP": "",
    "P": "49401204891",
    "ticket": "",
    "clientID": "1592032637185.3guqqy",
    "Frontend": {
      "vid": "1592032637185.3guqqy",
      "sessionID": 2,
      "pvid": 15
    },
    "Union": {
      "AllianceID": "",
      "SID": "",
      "Ouid": ""
    },
    "HotelExtension": {
      "group": "TRIP",
      "hasAidInUrl": "false",
      "Qid": 258063499226,
      "WebpSupport": "true",
      "PID": "8b55127e-e0bf-414c-ba1f-f4df70f8f963",
      "hotelUuidKey": "MamWf3w8QY5gK9LwLJMLJqLwOJLCYS9jaTjVQIqJM9r6nYC0W1sJOJpQiu0R7VioziUJQpiTNJfMvqAibJPSysUJbFr0fjm9vMSYgLJqXianrPJB4vCbr8Tx3ZjVCi93WgFjG1JUJ3J3GeX1YmPJUCvsZYqEZgY3si0DRqCJ3hRsuwSJPJagwZVKmPRShjmHWsbiOE1gJn5yUgr9EaLJdsI0Bv3zxLfYNVJPQx5lx6bJ6mjgaRupi0CwFdyUOICoiUELoyoTjZ0R4ovpAWbfiNSJFmi4E8Sr4SjuqR6hWabwGBi18xD0xnHwTnwOBwglIl3YNDRbowA4io3YXUjQ3J1gE7BYOPYUOx4pj9HjgVxVBIF5iAXYnZRFGK8PRhTjq6W1MiTsYoswZsr58vPQjzCwsqrqfvzaK4awfMK64ROmjnBWami0EzAJmpylurVE8nisXYG7i38KQgiTZRO5xVaxaTxDQx1owpNwbFw4XISBY9MRLmw9siu7Yh6jN6JDlEL1wuzr8AvUZj4dwNZrnuvg3KZ4I4Gwg4iLHYB0xnSiCEDowO6I7oYb9i3QJ8ui7VxqhxAGxZhx0fwBpwDLwOfIlzY87RTnw9liO9Y76jT4J3UEMzYXLY4LJ3dwS1Rb5IzGj33K8LwdnwuUw1uIqZYfaRpuwuVifAYXsjdMJmFE9NY0DYMNwF7J3SwH0R76IM9j9JGHJZ8jF1jSTjDAi1JsLioay8qvDNv5qINQJsoihLy6J5UWA5yLSvpNJfdIb8w3TJSbWN5rmuR7fisDWZsR4J6lJoTw5PyZHRN0yZ7RLZjz0JZPv7Nr6ZJg9RQ5IUHE15v0GwATybTJBJp7WmFyBpvhOJCnInHjX5JTlRS9jg7rB5Wz4iABy0J78idFRS3vTn",
      "hotelUuid": "PCDT5c3PudA60kHaqA"
    }
  }
}
payload_request_hotel = {
  "checkIn": "2020-11-18",
  "checkOut": "2020-11-25",
  "priceType": "0",
  "adult": 2,
  "popularFacilityType": "",
  "fgt": "",
  "hotelUniqueKey": "",
  "child": 0,
  "roomNum": 1,
  "masterHotelId": 436921,
  "age": "",
  "cityId": "1",
  "minCurr": "CNY",
  "minPrice": "554.00",
  "hotel": "436921",
  "Head": {
    "Locale": "en-XX",
    "Currency": "CNY",
    "AID": "",
    "SID": "",
    "ClientID": "1592032637185.3guqqy",
    "OUID": "",
    "TimeZone": "5.5",
    "PageID": "10320668147",
    "HotelExtension": {
      "WebpSupport": "true",
      "Qid": 258063499226,
      "hasAidInUrl": "false",
      "group": "TRIP",
      "PID": "8c9d0d0c-73df-43dc-9c19-4ecac5c72481",
      "hotelUuidKey": "",
      "hotelUuid": ""
    },
    "Frontend": {
      "vid": "",
      "sessionID": "",
      "pvid": ""
    },
    "P": "11123714724",
    "Device": "PC",
    "Version": 0
  }
}


def price_from_string(strin):
    return re.findall(r'\d*\.\d+|\d+', str(strin).replace(',', ''))[0]


# Extracting Hotel's data for every page
def extract_hotel(hotelID=None):
    try:
        if hotelID:
            payload_request_hotel['hotel'] = str(hotelID)
            payload_request_hotel['masterHotelId'] = int(hotelID)
            html_content = requests.post(hotel_detail, json=payload_request_hotel, headers=headers)
            soup_mob = html_content.json()
            all_room_type_count = len(soup_mob['Response']['baseRooms'])
            all_room_type = (soup_mob['Response']['baseRooms'])
            room_details = []
            for i in range(all_room_type_count):
                room_name = all_room_type[i]['baseRoom']['roomName']
                for j in range(len(all_room_type[i]['saleRoom'])):
                    room_details.append({
                        hotelID: [room_name, all_room_type[i]['saleRoom'][j]['facility'][0]['content'],
                                  price_from_string(all_room_type[i]['saleRoom'][j]['money']['priceNote'])]})
            return room_details
        else:
            return False
    except:
        return False


results = []


def get_hotels(num, q):
    html_content_main = requests.request('POST', hotel_list_item, json=payload_request, headers=headers)
    hotel_list_count = len(html_content_main.json()['Response']['hotelList']['list'])
    checkout = datetime.strptime(payload_request['searchCondition']['checkOut'], '%Y-%m-%d')
    checkin = datetime.strptime(payload_request['searchCondition']['checkIn'], '%Y-%m-%d')
    num_nights = (checkout - checkin).days
    for i in range(hotel_list_count):
        payload_request['searchCondition']['pageNo'] = num
        html_content_main = requests.request('POST', hotel_list_item, json=payload_request, headers=headers)
        hotel_list = html_content_main.json()['Response']['hotelList']['list']

        htl_data = extract_hotel(hotelID=hotel_list[i]['base']['hotelId'])
        if not htl_data:
            continue
        for k in range(len(htl_data)):
            for key, v in htl_data[k].items():
                dict_list = []
                dict_list.append('')
                dict_list.append('Beijing')
                dict_list.append(payload_request['searchCondition']['checkIn'])
                dict_list.append(num_nights)
                dict_list.append(payload_request['searchCondition']['adult'])
                dict_list.append(hotel_list[i]['base']['hotelName'])
                dict_list.append(hotel_list[i]['base']['star'])
                dict_list.extend(v)
                q.put(dict_list)


if __name__ == "__main__":
    # Also we can implement the mulitprocessing for the same.
    qcount = 0
    country_name = []  # List to store country_name
    city_name = []  # List to store city name
    date_checkin = []  # List to store checkin date
    num_night = []  # List to store num of nights
    adult_count = []  # List to store adult count
    hotel_name = []  # List to store hotel
    hotel_rating = []  # List to store hotel rating
    room_name = []  # List to store Room type
    bed_amenities = []  # List to store bed & amenities
    avg_price_per_room = []  # List to store avg price

    m = Manager()
    q = m.Queue()
    pool_tuple = [(x, q) for x in range(1, 50)]
    html_content_main = requests.request('POST', hotel_list_item, json=payload_request, headers=headers)
    # API is giving error of 433 code, So not able to hit, we can do the same by selenium
    hotel_list_count = len(html_content_main.json()['Response']['hotelList']['list'])
    with Pool(processes=10) as pool:
        results = pool.starmap(get_hotels, pool_tuple)

    while q.empty() is not True:
        qcount = qcount + 1
        queue_top = q.get()
        country_name.append(queue_top[0])
        city_name.append(queue_top[1])
        date_checkin.append(queue_top[2])
        num_night.append(queue_top[3])
        adult_count.append(queue_top[4])
        hotel_name.append(queue_top[5])
        hotel_rating.append(queue_top[6])
        room_name.append(queue_top[7])
        bed_amenities.append(queue_top[8])
        avg_price_per_room.append(queue_top[9])
    hotel_dataframe = pd.DataFrame({'Country': country_name, 'City': city_name, 'Date': date_checkin, 'Night':num_night,
                                    'Adult': adult_count,	'Hotel Name': hotel_name,
                                    'Hotel Star Rating': hotel_rating,	'Room Name': room_name,
                                    'Beds & Amenities': bed_amenities, 'Avg. Price Per Room / Night': avg_price_per_room})
    hotel_dataframe.to_csv('trip.csv', index=False, encoding='utf-8')
